import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '../store'
// import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    // component: Home
    component: () => import(/* webpackChunkName: "home" */ '../views/Home.vue')
  },  
  {
    path: '/blogs',
    name: 'Blogs',
    component: () => import(/* webpackChunkName: "blogs" */ '../views/Blogs.vue')
  },
  {
    path: '/store',
    name: 'Store',
    component: () => import(/* webpackChunkName: "store" */ '../views/Store.vue'),
    meta: {
      auth: true,
    }
  },
  {
    path: '/blog/:id',
    name: 'Blog',
    // Lazy Load
    component: () => import(/* webpackChunkName: "blog" */ '../views/Blog.vue')
  },
  {
    path: '/about',
    name: 'About',
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/login',
    name: 'auth.Login',
    component: () => import(/* webpackChunkName: "register" */ '../views/auth/Login.vue')
  },
  {
    path: '/register',
    name: 'auth.Register',
    component: () => import(/* webpackChunkName: "register" */ '../views/auth/Register.vue')
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  linkActiveClass: "active",
  routes
})

router.beforeEach((to, from, next) => {
  if(to.meta.auth && !store.getters['auth/check']) next({ name:'auth.Login' })
  else next()

  if(to.name == 'auth.Login' && store.getters['auth/check']) next({ name:'Home' })
  else next()
})

export default router
