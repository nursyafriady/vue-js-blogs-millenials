import Vue from 'vue'
import Vuex from 'vuex'
import auth from './auth'
import blogs from './blogs'
import VuexPersistence from 'vuex-persist'
 
const vuexPersist = new VuexPersistence({
  key: 'sanbercode',
  storage: localStorage
});

Vue.use(Vuex)

export default new Vuex.Store({
  plugins: [vuexPersist.plugin],
  modules : {
    auth,
    blogs
  },
})
